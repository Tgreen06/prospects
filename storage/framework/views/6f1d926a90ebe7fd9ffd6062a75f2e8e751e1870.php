<?php $__env->startSection('content'); ?>
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">

                <div class="panel-body">
                    <a href="/resource/create/">create</a>
                    <table class="table table-bordered">
                        <tr>
                            <th>
                                id
                            </th>
                            <th>
                                label
                            </th>
                            <th>
                                description
                            </th>
                            <th>
                                type id
                            </th>
                            <th>
                                status id
                            </th>
                            <th>
                                update
                            </th>
                            <th>
                                delete
                            </th>
                        </tr>
                        <?php $__currentLoopData = $resource; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $prospect): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                            <tr>
                                <td>
                                    <?php echo e($prospect->id); ?>

                                </td>
                                <td>
                                    <?php echo e($prospect->label); ?>

                                </td>
                                <td>
                                    <?php echo e($prospect->description); ?>

                                </td>
                                <td>
                                    <?php echo e($prospect->prospect_type_id); ?>

                                </td>
                                <td>
                                    <?php echo e($prospect->prospect_status_id); ?>

                                </td>
                                <td>
                                    <a href="/resource/update/<?php echo e($prospect->id); ?>">update</a>
                                </td>
                                <td>
                                    <a href="/resource/delete/<?php echo e($prospect->id); ?>">delete</a>
                                </td>
                            </tr>
                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                    </table>
                </div>

            </div>
        </div>
    </div>
</div>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.app', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
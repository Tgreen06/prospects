<?php $__env->startSection('content'); ?>
    <div class="container">

        <div class="row">

            <div class="col-md-8 col-md-offset-2">
                <div class="panel panel-default">
                    <a href="/prospects/create/">create</a>
                    <table id="example" class="display" cellspacing="0" width="100%">

                        <thead>
                        <tr>
                            <th>id</th>
                            <th>label</th>
                            <th>description</th>
                            <th>prospect_status_id</th>
                            <th>prospect_type_id</th>
                            <th>update</th>
                            <th>delete</th>
                        </tr>
                        </thead>

                    </table>

                </div>
            </div>
        </div>
    </div>
    <script>
        $(document).ready(function() {
            $('#example').DataTable({
                ajax: 'prospects/data',
                columns: [
                    { "data": "id" },
                    { "data": "label" },
                    { "data": "description" },
                    { "data": "prospect_type_id" },
                    { "data": "prospect_status_id" }

                ],
                "aoColumnDefs": [
                    {
                        "aTargets": [5],
                        "mData": "id",
                        "mRender": function (id) {
                            return '<a href="/prospects/update/' + id + '"' + 'id="'+ id + '">Edit</a>';
                            // oTable.fnUpdate( ['label', 'description', 'prospect_type_id','prospect_status_id', ... ], rowIndex );
                        }
                    },
                    {
                        "aTargets": [6],
                        "mData": "id",
                        "mRender": function (id) {
                            return '<a href="/prospects/delete/' + id + '"' + 'id="'+ id + '">delete</a>';
                            // oTable.fnUpdate( ['label', 'description', 'prospect_type_id','prospect_status_id', ... ], rowIndex );
                        }
                    }
                ]
            });


        });
    </script>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.app', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
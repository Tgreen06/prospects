<?php $__env->startSection('content'); ?>
    <div class="container">
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <form action="<?php echo e(route($route)); ?>" method="post">
                    <div>

                        <?php echo e(csrf_field()); ?>


                        <?php if($errors->any()): ?>
                            <div class="alert alert-danger">
                                <ul>
                                    <?php $__currentLoopData = $errors->all(); $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $error): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                        <li><?php echo e($error); ?></li>
                                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                </ul>
                            </div>
                        <?php endif; ?>

                        <input type="hidden" name="id" value="<?php if(isset($prospect)): ?> <?php echo e($prospect->id); ?> <?php endif; ?>">

                        <div class="form-group">
                            <label for="label">Label: </label>
                            <input class="form-control" type="text" name="label" id="label" value="<?php echo e(old('label')); ?>">
                        </div>

                        <div class="form-group">
                            <label for="description">Description:</label>
                            <textarea class="form-control" type="text" name="description" id="description" value="<?php echo e(old('description')); ?>"></textarea>
                        </div>


                        <div class="form-group">
                            <label for="prospect_type_id">Type:</label>
                            <select class="form-control" id="prospect_type_id" name="prospect_type_id">
                                <?php $__currentLoopData = $prospectTypes; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $type): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                    <option value="<?php echo e($type->id); ?>"><?php echo e($type->label); ?></option>
                                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                            </select>
                        </div>

                        <div class="form-group">
                            <label for="prospect_status_id">Status:</label>
                            <select class="form-control" id="prospect_status_id" name="prospect_status_id">
                                <?php $__currentLoopData = $prospectStatuses; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $status): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                    <option value="<?php echo e($status->id); ?>"><?php echo e($status->label); ?></option>
                                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                            </select>
                        </div>

                        <button class="btn btn-primary" type="submit" name="submit">Submit</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.app', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * Class Prospect_Types
 *
 * @package App
 */
class Prospect_Types extends Model
{
    /**
     * @var string
     */
    protected $table = 'prospect_types';

    /**
     * @var array $fillable
     */
    protected $fillable = [
        'id',
        'slug',
        'label',
        'description',
        'sort_order',
        'enabled'
    ];
}

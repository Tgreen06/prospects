<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * Class Prospects
 *
 * @package App
 */
class Prospects extends Model
{

    /**
     * @var bool $timestamps
     */
    public $timestamps = false;

    /**
     * @var string $table
     */
    protected $table = 'prospects';

    /**
     * @var string $label
     */
    protected $label;

    /**
     * @var string $description
     */
    protected $description;

    /**
     * @var int $prospect_status_id
     */
    protected $prospect_status_id;

    /**
     * @var int $prospect_type_id
     */
    protected $prospect_type_id;

    /**
     * @var array $fillable
     */
    protected $fillable = [

        'label',
        'description',
        'prospect_type_id',
        'prospect_status_id',
    ];
}

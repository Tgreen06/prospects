<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

/**
 * Class ErrorFormRequest
 *
 * @package App\Http\Requests
 */
class ErrorFormRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'label' => 'required|max:255',
            'description' => 'required',
            'prospect_type_id' => 'required|between:1,3',
            'prospect_status_id' => 'required|between:1,5'
        ];
    }
}

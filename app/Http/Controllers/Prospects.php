<?php

namespace App\Http\Controllers;

/**
 * Class Prospects
 *
 * @package App\Http\Controllers
 */
class Prospects extends Controller
{
    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        return view('Home/home');
    }
}

<?php

namespace App\Http\Controllers;

use App\Http\Requests\ErrorFormRequest;
use App\Prospect_Types;
use App\Prospect_Statuses;
use App\Prospects;
use Illuminate\Http\Request;

/**
 * Class Datatables
 *
 * @package App\Http\Controllers
 */
class Datatables extends Controller
{
    /**
     * @return array
     */
    public function data()
    {
        $prospects = new Prospects();

        return ['data' => $prospects->get()->toArray()];
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        return view('DataTable/prospects');
    }

    /**
     * @param ErrorFormRequest $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update(ErrorFormRequest $request)
    {
        $prospects = new Prospects;

        /** @var Prospects $prospect */
        $prospect = $prospects->find($request->get('id'));
        $prospect->label = $request->get('label');
        $prospect->description = $request->get('description');
        $prospect->prospect_status_id = $request->get('prospect_status_id');
        $prospect->prospect_type_id = $request->get('prospect_type_id');
        $prospect->save();

        return redirect('DataTable/prospects');
    }

    /**
     * @param int $id
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function updateForm($id)
    {
        $prospectStatuses = Prospect_Statuses::all();
        $prospectTypes = Prospect_Types::all();

        $prospects = new Prospects;

        return view('userForms/form',
        [
            'route' => 'datatables.update',
            'prospect' => $prospects->find($id),
            'prospectStatuses' => $prospectStatuses,
            'prospectTypes' => $prospectTypes
        ]);
    }

    /**
     * @param Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function delete(Request $request)
    {
        $prospects = new Prospects;

        $prospect = $prospects->find($request->id);

        $prospect->delete();

        return redirect('prospects');
    }

    /**
     * @param ErrorFormRequest $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function create(ErrorFormRequest $request)
    {
        $prospects = new Prospects;

        $prospects->label = $request->get('label');
        $prospects->description = $request->get('description');
        $prospects->prospect_status_id = $request->get('prospect_status_id');
        $prospects->prospect_type_id = $request->get('prospect_type_id');
        $prospects->save();

        return redirect('DataTable/prospects');
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function createForm()
    {
        $prospectStatuses = Prospect_Statuses::all();
        $prospectTypes = Prospect_Types::all();

        return view('userForms/form',
        [
            'route' => 'datatables.create',
            'prospectStatuses' => $prospectStatuses,
            'prospectTypes' => $prospectTypes
        ]);
    }
}

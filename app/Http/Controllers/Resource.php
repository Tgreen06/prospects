<?php

namespace App\Http\Controllers;

use App\Http\Requests\ErrorFormRequest;
use App\Prospect_Statuses;
use App\Prospect_Types;
use App\Prospects;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;


/**
 * Class Resource
 *
 * @package App\Http\Controllers
 */
class Resource extends Controller
{

    /**
     * @return $this
     */
    public function index()
    {
        $prospects = new Prospects();
        $resource = $prospects::all();

        return view('ProsTable/resource')->with('resource', $resource);
    }

    /**
     * @param ErrorFormRequest $request
     *
     * @return $this
     */
    public function create(ErrorFormRequest $request)
    {

        $prospects = new Prospects;

        $prospects->label = $request->get('label');
        $prospects->description = $request->get('description');
        $prospects->prospect_status_id = $request->get('prospect_status_id');
        $prospects->prospect_type_id = $request->get('prospect_type_id');
        $prospects->save();

        $resource = $prospects::all();

        return view('ProsTable/resource')->with('resource', $resource);
    }

    /**
     * @param ErrorFormRequest $request
     *
     * @return $this
     */
    public function update(ErrorFormRequest $request)
    {
        $prospects = new Prospects;

        $prospect = $prospects->find($request->get('id'));

        $prospect->label = $request->get('label');
        $prospect->description = $request->get('description');
        $prospect->prospect_status_id = $request->get('prospect_status_id');
        $prospect->prospect_type_id = $request->get('prospect_type_id');
        $prospect->save();

        $resource = $prospects::all();

        return view('ProsTable/resource')->with('resource', $resource);
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function createForm()
    {
        $prospectStatuses = Prospect_Statuses::all();
        $prospectTypes = Prospect_Types::all();

        return view('userForms/form',
        [
            'route' => 'resource.create',
            'prospectStatuses' => $prospectStatuses,
            'prospectTypes' => $prospectTypes
        ]);
    }

    /**
     * @param $id
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function updateForm($id)
    {
        $prospectStatuses = Prospect_Statuses::all();
        $prospectTypes = Prospect_Types::all();

        $prospects = new Prospects;

        return view('userForms/form',
        [
            'route' => 'resource.update',
            'prospect' => $prospects->find($id),
            'prospectStatuses' => $prospectStatuses,
            'prospectTypes' => $prospectTypes
        ]);
    }

    /**
     * @param Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function delete(Request $request)
    {
        $prospects = new Prospects;

        $prospect = $prospects->find($request->id);

        $prospect->delete();

        return redirect('resource');
    }

    /**
     * @return mixed
     */
    public function getSignOut()
    {
        Auth::logout();

        return Redirect::route('home');
    }
}

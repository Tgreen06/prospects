<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * Class Prospect_Statuses
 *
 * @package App
 */
class Prospect_Statuses extends Model
{
    /**
     * @var string $table
     */
    protected $table = 'prospect_statuses';

    /**
     * @var array $fillable
     */
    protected $fillable = [
        'id',
        'slug',
        'label',
        'description',
        'sort_order',
        'enabled'
    ];
}

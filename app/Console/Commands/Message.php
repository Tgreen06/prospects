<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Prospects;


class Message extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'command:project';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     *
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     *
     */
    public function handle()
    {

        $this->create();
        $this->retrieve();
        $this->last();
        $this->update();
        $this->remove();
    }

    /**
     * asks and creates desired amount of prospects
     */
    private function create()
    {
        /** @var  $answer */

        $answer = $this->ask('How many prospects do you want to create');

        for($i = 1;$i <= $answer;$i++)
        {
            $prospects = new Prospects();

            $prospects->label = str_random(10);
            $prospects->description = str_random(30);
            $prospects->prospect_type_id = random_int(1,3);
            $prospects->prospect_status_id = random_int(1,5);
            $prospects->save();

        }
        echo "$answer prospects created \n";

    }

    /**
     *  Retrieves columns ID & Label & prints them.
     */
    public function retrieve()
    {
        echo "Prospect List \n";
        $prospects = new Prospects();

        $results = $prospects->orderby('id','asc')->orderby('label')->take(5)->get();

        foreach ($results as $line) {
            $id = $line->id;
            $label = $line->label;
            echo "$id $label \n";
        }
    }

    /**
     * Find the last ID in the database and brings back the label of that ID.
     */
    public function last()
    {
        echo "\n";

        $prospects = new Prospects();

        $results = $prospects->orderBy('id',"desc")->first();

        echo "This is the last id: $results->id \n";

        $recorded = $results->id;

        echo "this is the label of the last id: $recorded \n";

    }

    /**
     *
     */
    public function update()
    {
        /** @var  $prospects */
        $prospects = new Prospects();

        $answer = $this->ask('Which IDs Label would you like to update');

        $prospects = $prospects->find($answer);

        $label = $this->ask('Update the label of the chosen ID');

        $prospects->label = $label;

        $prospects->save();

        echo "ID Selected : $answer \n Updated the label as follows : $label \n" ;
    }

    /**
     *
     */
    public function remove()
    {
        echo "\n";

        $prospects = new Prospects();

        $prospects->truncate();

        echo "records deleted \n";
    }

}

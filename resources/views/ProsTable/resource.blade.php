@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">

                <div class="panel-body">
                    <a href="/resource/create/">create</a>
                    <table class="table table-bordered">
                        <tr>
                            <th>
                                id
                            </th>
                            <th>
                                label
                            </th>
                            <th>
                                description
                            </th>
                            <th>
                                type id
                            </th>
                            <th>
                                status id
                            </th>
                            <th>
                                update
                            </th>
                            <th>
                                delete
                            </th>
                        </tr>
                        @foreach($resource as $prospect)
                            <tr>
                                <td>
                                    {{$prospect->id}}
                                </td>
                                <td>
                                    {{$prospect->label}}
                                </td>
                                <td>
                                    {{$prospect->description}}
                                </td>
                                <td>
                                    {{$prospect->prospect_type_id}}
                                </td>
                                <td>
                                    {{$prospect->prospect_status_id}}
                                </td>
                                <td>
                                    <a href="/resource/update/{{$prospect->id}}">update</a>
                                </td>
                                <td>
                                    <a href="/resource/delete/{{$prospect->id}}">delete</a>
                                </td>
                            </tr>
                        @endforeach
                    </table>
                </div>

            </div>
        </div>
    </div>
</div>
@endsection

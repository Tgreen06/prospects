@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <form action="{{route($route)}}" method="post">
                    <div>

                        {{ csrf_field() }}

                        @if ($errors->any())
                            <div class="alert alert-danger">
                                <ul>
                                    @foreach ($errors->all() as $error)
                                        <li>{{ $error }}</li>
                                    @endforeach
                                </ul>
                            </div>
                        @endif

                        <input type="hidden" name="id" value="@if(isset($prospect)) {{$prospect->id}} @endif">

                        <div class="form-group">
                            <label for="label">Label: </label>
                            <input class="form-control" type="text" name="label" id="label" value="{{ old('label') }}">
                        </div>

                        <div class="form-group">
                            <label for="description">Description:</label>
                            <textarea class="form-control" type="text" name="description" id="description" value="{{ old('description') }}"></textarea>
                        </div>


                        <div class="form-group">
                            <label for="prospect_type_id">Type:</label>
                            <select class="form-control" id="prospect_type_id" name="prospect_type_id">
                                @foreach($prospectTypes as $type)
                                    <option value="{{$type->id}}">{{$type->label}}</option>
                                @endforeach
                            </select>
                        </div>

                        <div class="form-group">
                            <label for="prospect_status_id">Status:</label>
                            <select class="form-control" id="prospect_status_id" name="prospect_status_id">
                                @foreach($prospectStatuses as $status)
                                    <option value="{{$status->id}}">{{$status->label}}</option>
                                @endforeach
                            </select>
                        </div>

                        <button class="btn btn-primary" type="submit" name="submit">Submit</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection

<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes();

Route::get('/home', function () {
  return redirect('/');
});

Route::get('/', 'HomeController@index')->name('home')->middleware('auth');

#Resources Routes
Route::get('/resource', 'Resource@index')->name('resource')->middleware('auth');

Route::get('/resource/create', 'Resource@createForm')->middleware('auth');

Route::post('/resource/create', 'Resource@create')->name('resource.create')->middleware('auth');

Route::get('/resource/update/{id}', 'Resource@updateForm')->middleware('auth');

Route::get('/resource/delete/{id}', 'Resource@delete')->name('resource.delete')->middleware('auth');

Route::post('/resource', 'Resource@update')->name('resource.update')->middleware('auth');

#Datatable Routes
Route::get('/prospects', 'Datatables@index')->name('datatables')->middleware('auth');

Route::get('/prospects/data', 'Datatables@data')->middleware('auth');

Route::get('/prospects/delete/{id}', 'Datatables@delete')->name('datatables.delete')->middleware('auth');

Route::post('/prospects/update', 'Datatables@update')->name('datatables.update')->middleware('auth');

Route::get('/prospects/update/{id}', 'Datatables@updateForm')->middleware('auth');

Route::get('/prospects/create', 'Datatables@createForm')->middleware('auth');

Route::post('/prospects', 'Datatables@create')->name('datatables.create')->middleware('auth');

